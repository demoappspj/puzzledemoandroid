package com.jasiak.puzzledemoapp.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

public class SimplePuzzleGridView extends GridView {
    public SimplePuzzleGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
