package com.jasiak.puzzledemoapp.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.jasiak.puzzledemoapp.DemoAppConstants;
import com.jasiak.puzzledemoapp.logic.CommonAxis;
import com.jasiak.puzzledemoapp.logic.PuzzleArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.widget.ImageView.ScaleType.CENTER_CROP;

public class PuzzleGridAdapter extends BaseAdapter {
    private List<Bitmap> elements = new ArrayList<>();
    private Map<Bitmap, Integer> elementsIdMap = new HashMap<>();
    private int ELEMENTS_ID_COUNTER = 0;
    private Context context;
    private PuzzleArray puzzleArray;

    public PuzzleGridAdapter(Context context, List<Bitmap> elements, Bitmap emptySpaceBitmap) {
        initialize(elements, emptySpaceBitmap);
        this.context = context;
    }

    public void swapElements(int sourcePosition, int destinationPosition) {
        Bitmap sourceElement = (Bitmap) getItem(sourcePosition);
        Bitmap destinationElement = (Bitmap) getItem(destinationPosition);
        puzzleArray.swapElements(sourceElement, destinationElement);
        refreshViewData();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            return getNewView(position);
        }
        return view;
    }

    @Override
    public int getCount() {
        return elements.size();
    }

    @Override
    public Object getItem(int position) {
        return elements.get(position);
    }

    @Override
    public final boolean hasStableIds() {
        return true;
    }

    @Override
    public final long getItemId(int position) {
        if (position < 0 || position >= elementsIdMap.size()) {
            return DemoAppConstants.OUT_OF_BOUNDARIES_ID;
        }
        Object item = getItem(position);
        return elementsIdMap.get(item);
    }


    public int getEmptySpaceTilePosition() {
        Bitmap emptySpaceElement = puzzleArray.getEmptySpaceElement();
        return puzzleArray.getPositionOfElement(emptySpaceElement);
    }

    public void swapWithEmptySpaceElement(int position) {
        swapElements(position, getEmptySpaceTilePosition());
    }

    public void multiElementSwapWithEmptySpaceElement(int position) {
        Bitmap element = (Bitmap) getItem(position);
        puzzleArray.multiElementSwapWithEmptySpaceElement(element);
        refreshViewData();
    }

    public boolean isStartSwapFromElementAllowed(int tilePosition) {
        Bitmap givenTile = (Bitmap) getItem(tilePosition);
        return puzzleArray.isStartSwapFromElementAllowed(givenTile);
    }

    public boolean isEmptySpaceElementInTheSameRowOrColumn(int position) {
        Bitmap element = (Bitmap) getItem(position);
        if (puzzleArray.getEmptySpaceElementInTheSameRowOrColumn(element).equals(CommonAxis.X)
                || puzzleArray.getEmptySpaceElementInTheSameRowOrColumn(element).equals(CommonAxis.Y)) {
            return true;
        }
        return false;
    }

    protected void initialize(List<Bitmap> elements, Bitmap emptySpaceBitmap) {
        puzzleArray = new PuzzleArray(elements, emptySpaceBitmap);
        initializeElementsIdMap(puzzleArray.getAsList());
        this.elements.addAll(puzzleArray.getAsList());
    }

    protected void initializeElementsIdMap(List<Bitmap> elements) {
        for (Bitmap element : elements) {
            addStableId(element);
        }
    }

    protected void addStableId(Bitmap element) {
        elementsIdMap.put(element, ELEMENTS_ID_COUNTER++);
    }

    private ImageView getNewView(int position) {
        Bitmap element = elements.get(position);
        ImageView view = new ImageView(context);
        view.setScaleType(CENTER_CROP);
        view.setLayoutParams(new GridView.LayoutParams(element.getWidth(), element.getHeight()));
        view.setPadding(1, 1, 1, 1);
        view.setImageDrawable(new BitmapDrawable(context.getResources(), element));
        return view;
    }

    private void refreshViewData() {
        elements.clear();
        elements.addAll(puzzleArray.getAsList());
        notifyDataSetChanged();
    }

}