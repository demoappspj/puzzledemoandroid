package com.jasiak.puzzledemoapp.view;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;

import com.jasiak.puzzledemoapp.R;
import com.jasiak.puzzledemoapp.logic.PuzzleImagesGenerator;

import static com.jasiak.puzzledemoapp.DemoAppConstants.PUZZLE_ARRAY_DIMENSION;
import static com.jasiak.puzzledemoapp.DemoAppConstants.SCALE_FACTOR;

public class PuzzleActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreenLayout();
        setContentView(R.layout.puzzle_activity);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initPuzzleView();
    }

    private void setFullScreenLayout() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void initPuzzleView() {
        SimplePuzzleGridView gridView = (SimplePuzzleGridView) findViewById(R.id.puzzle_grid);
        Point displaySize = new Point();
        getWindowManager().getDefaultDisplay().getSize(displaySize);
        PuzzleImagesGenerator task = new PuzzleImagesGenerator(getApplicationContext(), gridView, (int) (displaySize.x * SCALE_FACTOR), (int) (displaySize.y * SCALE_FACTOR));
        task.execute((long) (PUZZLE_ARRAY_DIMENSION * PUZZLE_ARRAY_DIMENSION));
        attachListenersToGridView(gridView);
    }

    private void attachListenersToGridView(SimplePuzzleGridView gridView) {
        gridView.setOnItemClickListener(onItemClickListener(gridView));
    }

    private SimplePuzzleGridView.OnItemClickListener onItemClickListener(final SimplePuzzleGridView gridView) {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PuzzleGridAdapter adapter = (PuzzleGridAdapter) gridView.getAdapter();
                int emptySpaceTilePosition = adapter.getEmptySpaceTilePosition();
                Log.d("PuzzleActivity", "Empty element position: " + emptySpaceTilePosition);
                if (adapter.isStartSwapFromElementAllowed(position)) {
                    handleSingleSwap(position, adapter);
                } else if (adapter.isEmptySpaceElementInTheSameRowOrColumn(position)) {
                    handleMultiElementSwap(position, adapter);
                } else {
                    Log.d("PuzzleActivity", "Swap from position: " + position + " NOT allowed");
                }
            }
        };
    }

    private void handleMultiElementSwap(int position, PuzzleGridAdapter adapter) {
        Log.d("PuzzleActivity", "MultiElement Swap from position: " + position + " allowed");
        adapter.multiElementSwapWithEmptySpaceElement(position);
    }

    private void handleSingleSwap(int position, PuzzleGridAdapter adapter) {
        Log.d("PuzzleActivity", "Swap from position: " + position + " allowed");
        adapter.swapWithEmptySpaceElement(position);
    }

}