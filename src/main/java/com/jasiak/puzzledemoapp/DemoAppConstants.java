package com.jasiak.puzzledemoapp;

public class DemoAppConstants {
    public static final int PUZZLE_ARRAY_DIMENSION = 4;
    public static final int EMPTY_SPACE_ELEMENT_INDEX = 15;
    public static final int OUT_OF_BOUNDARIES_ID = -1;
    public static final double SCALE_FACTOR = 0.95;
}
