package com.jasiak.puzzledemoapp.logic;

import android.graphics.Bitmap;
import android.util.Pair;

import java.util.ArrayList;
import java.util.List;

import static com.jasiak.puzzledemoapp.DemoAppConstants.OUT_OF_BOUNDARIES_ID;
import static com.jasiak.puzzledemoapp.DemoAppConstants.PUZZLE_ARRAY_DIMENSION;
import static com.jasiak.puzzledemoapp.logic.CommonAxis.NONE;
import static com.jasiak.puzzledemoapp.logic.CommonAxis.X;
import static com.jasiak.puzzledemoapp.logic.CommonAxis.Y;

public class PuzzleArray {

    private Bitmap[][] elementsArray;
    private final Bitmap emptySpaceElement;

    public PuzzleArray(List<Bitmap> elements, Bitmap emptySpaceElement) {
        super();
        elementsArray = new Bitmap[PUZZLE_ARRAY_DIMENSION][PUZZLE_ARRAY_DIMENSION];
        addElementsToArray(elements);
        this.emptySpaceElement = emptySpaceElement;
    }

    public boolean isStartSwapFromElementAllowed(Bitmap element) {
        Pair<Integer, Integer> position = getXyForBitmapElement(element);
        List<Pair<Integer, Integer>> neighbors = getNeighbors(position.first, position.second, PUZZLE_ARRAY_DIMENSION, PUZZLE_ARRAY_DIMENSION);
        List<Bitmap> neighborsElements = convertNeighborsXyToBitmapElementsList(neighbors);
        return neighborsElements.contains(emptySpaceElement) ? true : false;
    }


    public void swapElements(final Bitmap sourceElement,final Bitmap destinationElement) {
        Pair<Integer, Integer> sourceElementXy = getXyForBitmapElement(sourceElement);
        Pair<Integer, Integer> destinationElementXy = getXyForBitmapElement(destinationElement);
        elementsArray[destinationElementXy.first][destinationElementXy.second] = sourceElement;
        elementsArray[sourceElementXy.first][sourceElementXy.second] = destinationElement;
    }


    public Bitmap getEmptySpaceElement() {
        return emptySpaceElement;
    }

    public int getPositionOfElement(Bitmap element) {
        int elementIndex = 0;
        for (int y = 0; y < PUZZLE_ARRAY_DIMENSION; y++) {
            for (int x = 0; x < PUZZLE_ARRAY_DIMENSION; x++) {
                if (elementsArray[x][y] == element)
                    return elementIndex;
                elementIndex++;
            }
        }
        return OUT_OF_BOUNDARIES_ID;
    }

    public List<Bitmap> getAsList() {
        List<Bitmap> list = new ArrayList<>();
        for (int y = 0; y < PUZZLE_ARRAY_DIMENSION; y++) {
            for (int x = 0; x < PUZZLE_ARRAY_DIMENSION; x++) {
                list.add(elementsArray[x][y]);
            }
        }
        return list;
    }

    public CommonAxis getEmptySpaceElementInTheSameRowOrColumn(Bitmap element) {
        Pair<Integer, Integer> xyForGivenElement = getXyForBitmapElement(element);
        Pair<Integer, Integer> xyForEmptyElement = getXyForBitmapElement(emptySpaceElement);
        if (xyForEmptyElement.first.equals(xyForGivenElement.first))
            return X;
        if (xyForEmptyElement.second.equals(xyForGivenElement.second))
            return Y;
        return NONE;
    }

    public void multiElementSwapWithEmptySpaceElement(Bitmap element) {
        CommonAxis commonAxis = getEmptySpaceElementInTheSameRowOrColumn(element);
        Pair<Integer, Integer> xyForEmptyElement = getXyForBitmapElement(emptySpaceElement);
        Pair<Integer, Integer> xyForBitmapElement = getXyForBitmapElement(element);
        if (commonAxis.equals(X)) {
            handleXAxisMultiElementMove(xyForEmptyElement, xyForBitmapElement);
        } else if (commonAxis.equals(Y)) {
            handleYAxisMultiElementMove(xyForEmptyElement, xyForBitmapElement);
        }
    }

    private void handleYAxisMultiElementMove(Pair<Integer, Integer> xyForEmptyElement, Pair<Integer, Integer> xyForBitmapElement) {
        int levelsToMove = xyForEmptyElement.first - xyForBitmapElement.first;
        if (levelsToMove > 0) {
            for (int i = 0; i < levelsToMove; i++)
                moveLeftEmptySpaceElement();
        } else {
            for (int i = levelsToMove; i < 0; i++)
                moveRightEmptySpaceElement();
        }
    }

    private void handleXAxisMultiElementMove(Pair<Integer, Integer> xyForEmptyElement, Pair<Integer, Integer> xyForBitmapElement) {
        int levelsToMove = xyForEmptyElement.second - xyForBitmapElement.second;
        if (levelsToMove > 0) {
            for (int i = 0; i < levelsToMove; i++)
                moveUpEmptySpaceElement();
        } else {
            for (int i = levelsToMove; i < 0; i++)
                moveDownEmptySpaceElement();
        }
    }

    private void addElementsToArray(List<Bitmap> elements) {
        int elementIndex = 0;
        for (int y = 0; y < PUZZLE_ARRAY_DIMENSION; y++) {
            for (int x = 0; x < PUZZLE_ARRAY_DIMENSION; x++) {
                elementsArray[x][y] = elements.get(elementIndex++);
            }
        }
    }

    private List<Pair<Integer, Integer>> getNeighbors(int x, int y, int maxX, int maxY) {
        ArrayList<Pair<Integer, Integer>> neighbors = new ArrayList<>();
        if (x > 0)
            neighbors.add(new Pair<>(x - 1, y));
        if (y > 0)
            neighbors.add(new Pair<>(x, y - 1));
        if (x < maxX)
            neighbors.add(new Pair<>(x + 1, y));
        if (x < maxY)
            neighbors.add(new Pair<>(x, y + 1));
        return neighbors;
    }

        private List<Bitmap> convertNeighborsXyToBitmapElementsList(List<Pair<Integer, Integer>> neighbors) {
        List<Bitmap> bitmapList = new ArrayList<>();
        for (Pair<Integer, Integer> neighbor : neighbors) {
            addIfFitsToBoundaries(bitmapList, neighbor);
        }
        return bitmapList;
    }

    private void addIfFitsToBoundaries(List<Bitmap> bitmapList, Pair<Integer, Integer> neighbor) {
        Integer x = neighbor.first;
        Integer y = neighbor.second;
        if (x < PUZZLE_ARRAY_DIMENSION && y < PUZZLE_ARRAY_DIMENSION)
            bitmapList.add(elementsArray[x][y]);
    }

    private Pair<Integer, Integer> getXyForBitmapElement(Bitmap element) {
        for (int x = 0; x < PUZZLE_ARRAY_DIMENSION; x++) {
            for (int y = 0; y < PUZZLE_ARRAY_DIMENSION; y++) {
                if (elementsArray[x][y] == element)
                    return new Pair<>(x, y);
            }
        }
        return null;
    }

    private void moveRightEmptySpaceElement() {
        Pair<Integer, Integer> xyForEmptyElement = getXyForBitmapElement(getEmptySpaceElement());
        Bitmap elementOnTheRightSide = elementsArray[xyForEmptyElement.first + 1][xyForEmptyElement.second];
        swapElements(emptySpaceElement, elementOnTheRightSide);
    }

    private void moveLeftEmptySpaceElement() {
        Pair<Integer, Integer> xyForEmptyElement = getXyForBitmapElement(getEmptySpaceElement());
        Bitmap elementOnTheLeftSide = elementsArray[xyForEmptyElement.first - 1][xyForEmptyElement.second];
        swapElements(emptySpaceElement, elementOnTheLeftSide);
    }

    private void moveDownEmptySpaceElement() {
        Pair<Integer, Integer> xyForEmptyElement = getXyForBitmapElement(getEmptySpaceElement());
        Bitmap ElementLower = elementsArray[xyForEmptyElement.first][xyForEmptyElement.second + 1];
        swapElements(emptySpaceElement, ElementLower);
    }

    private void moveUpEmptySpaceElement() {
        Pair<Integer, Integer> xyForEmptyElement = getXyForBitmapElement(getEmptySpaceElement());
        Bitmap elementUpper = elementsArray[xyForEmptyElement.first][xyForEmptyElement.second - 1];
        swapElements(emptySpaceElement, elementUpper);

    }
}