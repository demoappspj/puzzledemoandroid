
package com.jasiak.puzzledemoapp.logic;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.GridView;

import com.jasiak.puzzledemoapp.R;
import com.jasiak.puzzledemoapp.view.PuzzleGridAdapter;

import java.util.ArrayList;
import java.util.List;

import static com.jasiak.puzzledemoapp.DemoAppConstants.EMPTY_SPACE_ELEMENT_INDEX;
import static com.jasiak.puzzledemoapp.DemoAppConstants.PUZZLE_ARRAY_DIMENSION;

public class PuzzleImagesGenerator extends AsyncTask<Long, Integer, List<Bitmap>> {

    List<Bitmap> generatedElements = new ArrayList<>();
    private GridView gridView;
    private Context context;
    private int elementWidth;
    private int elementHeight;
    private int screenWidth;
    private int screenHeight;
    private Bitmap emptySpaceBitmap;

    public PuzzleImagesGenerator(Context context, GridView gridView, int screenWidth, int screenHeight) {
        this.context = context;
        this.gridView = gridView;
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
    }

    @Override
    protected List<Bitmap> doInBackground(Long[] params) {
        Bitmap inputBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.globe);
        Bitmap scaled = Bitmap.createScaledBitmap(inputBitmap, screenWidth, screenHeight, true);
        createPuzzleElementsImages(scaled);
        addEmptySpaceElement();
        return generatedElements;
    }

    @Override
    protected void onPostExecute(List<Bitmap> elements) {
        PuzzleGridAdapter adapter = new PuzzleGridAdapter(context, elements, emptySpaceBitmap);
        gridView.setAdapter(adapter);
        gridView.setNumColumns(PUZZLE_ARRAY_DIMENSION);
    }

    private void createPuzzleElementsImages(Bitmap sourceImage) {
        int sourceImageWidth = sourceImage.getWidth();
        int sourceImageHeight = sourceImage.getHeight();
        elementWidth = sourceImageWidth / PUZZLE_ARRAY_DIMENSION;
        elementHeight = sourceImageHeight / PUZZLE_ARRAY_DIMENSION;
        for (int y = 0; y + elementHeight <= sourceImageHeight; y += elementHeight) {
            for (int x = 0; x + elementWidth <= sourceImageWidth; x += elementWidth) {
                Bitmap tile = Bitmap.createBitmap(sourceImage, x, y, elementWidth, elementHeight);
                generatedElements.add(tile);
            }
        }
    }

    private void addEmptySpaceElement() {
        emptySpaceBitmap = Bitmap.createBitmap(elementWidth, elementHeight, Bitmap.Config.ARGB_8888);
        generatedElements.set(EMPTY_SPACE_ELEMENT_INDEX, emptySpaceBitmap);
    }
}